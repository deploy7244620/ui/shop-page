import {
    createUserWithEmailAndPassword,
    GoogleAuthProvider,
    signInWithRedirect,
} from 'firebase/auth';
import { auth } from '../../utils/firebase';
import { Loader } from 'lucide-react';
import { useState } from 'react';
import { FcGoogle } from 'react-icons/fc';
import { Link } from 'react-router-dom';
import InputText from '../../components/Input/InputText';
import ErrorText from '../../components/Typography/ErrorText';
import { useAuthState } from 'react-firebase-hooks/auth';

function Login() {
    const [user] = useAuthState(auth);
    console.log(user);
    if (user) {
        window.location.href = '/app/dashboard';
    }
    const INITIAL_LOGIN_OBJ = {
        password: '',
        email: '',
    };
    const [isEmailSigningIn, setIsEmailSigningIn] = useState(false);
    const [isGoogleSigningIn, setIsGoogleSigningIn] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [loginObj, setLoginObj] = useState(INITIAL_LOGIN_OBJ);
    const submitForm = e => {
        e.preventDefault();
        setErrorMessage('');
        console.log(loginObj);
        if (!isEmailSigningIn && !isGoogleSigningIn) {
            if (loginObj.email.trim() === '') return setErrorMessage('Email is required!');
            if (loginObj.password.trim() === '') return setErrorMessage('Password is required!');
            else {
                setIsEmailSigningIn(true);
                createUserWithEmailAndPassword(auth, loginObj.email, loginObj.password).catch(
                    err => {
                        switch (err.code) {
                            case 'auth/email-already-in-use':
                                setErrorMessage('Email already in use');
                                break;
                            case 'auth/weak-password':
                                setErrorMessage('Password is too weak');
                                break;
                            case 'auth/invalid-email':
                                setErrorMessage('Invalid email');
                                break;
                            default:
                                setErrorMessage(err.message);
                                break;
                        }
                        setIsEmailSigningIn(false);
                    },
                );
            }
        }
    };
    const onGoogleSignIn = () => {
        if (!isGoogleSigningIn && !isEmailSigningIn) {
            setIsGoogleSigningIn(true);
            const provider = new GoogleAuthProvider();
            signInWithRedirect(auth, provider).catch(e => {
                setErrorMessage(e.message);
                setIsGoogleSigningIn(false);
            });
        }
    };

    const updateFormValue = ({ updateType, value }) => {
        setErrorMessage('');
        setLoginObj({ ...loginObj, [updateType]: value });
    };

    return (
        <div className="flex min-h-screen items-center bg-base-200">
            <div className="card mx-auto w-full max-w-2xl shadow-xl">
                <div className="grid grid-cols-1 rounded-xl bg-base-100">
                    {/*<div>*/}
                    {/*    <LandingIntro />*/}
                    {/*</div>*/}
                    <div className="mt-10 px-10 pb-24">
                        <h1 className="mb-4 text-center text-3xl font-bold">
                            <img
                                src="/logo192.png"
                                className="mask mask-circle mr-2 inline-block w-12"
                                alt="hChip-logo"
                            />
                            hChip
                        </h1>
                        <form onSubmit={submitForm}>
                            <div className="mb-4">
                                <InputText
                                    autoFocus={true}
                                    type="email"
                                    placeholder={'Vui lòng nhập email của bạn'}
                                    updateType="email"
                                    containerStyle="mt-4"
                                    labelTitle="Email"
                                    updateFormValue={updateFormValue}
                                />
                                <InputText
                                    defaultValue={loginObj.password}
                                    type="password"
                                    placeholder={'Vui lòng nhập mật khẩu'}
                                    updateType="password"
                                    containerStyle="mt-4"
                                    labelTitle="Mật khẩu"
                                    updateFormValue={updateFormValue}
                                />
                            </div>
                            <div className="text-right text-primary">
                                <Link to="/forgot-password">
                                    <span className="inline-block  text-sm  transition duration-200 hover:cursor-pointer hover:text-primary hover:underline">
                                        Quên mật khẩu?
                                    </span>
                                </Link>
                            </div>
                            <ErrorText styleClass="mt-8">{errorMessage}</ErrorText>
                            <button type="submit" className={'btn btn-primary mt-2 w-full'}>
                                {isEmailSigningIn ? (
                                    <Loader className="mr-2 h-4 animate-spin" />
                                ) : (
                                    <> </>
                                )}
                                Đăng nhập
                            </button>
                            {/* make login with Google button */}
                            <button
                                type="button"
                                className="btn mt-2 w-full bg-gray-200 text-black hover:cursor-pointer hover:bg-gray-300 dark:bg-gray-800 dark:text-white dark:hover:bg-gray-700"
                                onClick={onGoogleSignIn}
                            >
                                {isGoogleSigningIn ? (
                                    <Loader className="mr-2 h-4 animate-spin" />
                                ) : (
                                    <FcGoogle className="h-6 w-6" />
                                )}
                                Đăng nhập với Google
                            </button>
                            <div className="mt-4 text-center">
                                Chưa có tài khoản?{' '}
                                <Link to="/register">
                                    <span className="  inline-block  transition duration-200 hover:cursor-pointer hover:text-primary hover:underline">
                                        Đăng ký
                                    </span>
                                </Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Login;
