import React, { useState } from 'react';

function Modal() {
    const [showModal, setShowModal] = useState(false);
    return (
        <>
            <button
                className="btn btn-primary btn-sm rounded-lg"
                type="button"
                onClick={() => setShowModal(true)}
            >
                In mã QR
            </button>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-50 flex items-center justify-center overflow-y-auto overflow-x-hidden outline-none focus:outline-none">
                        <div className="relative mx-auto my-6 w-auto max-w-3xl">
                            {/*content*/}
                            <div className="relative flex w-full flex-col rounded-lg border-0 bg-white shadow-lg outline-none focus:outline-none">
                                {/*header*/}
                                <div className=" border-blueGray-200 flex flex-row items-center justify-center rounded-t border-b border-solid p-5">
                                    <h3 className="text-3xl font-semibold">Quét để thanh toán</h3>
                                </div>
                                {/*body*/}
                                <div className="relative flex-auto p-6">
                                    <img src="/QR.png"></img>
                                </div>
                                {/*footer*/}
                                <div className="border-blueGray-200 flex items-center justify-end rounded-b border-t border-solid p-6">
                                    <button
                                        className="btn btn-primary btn-sm rounded-lg"
                                        type="button"
                                        onClick={() => setShowModal(false)}
                                    >
                                        Đóng
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="fixed inset-0 z-40 bg-black opacity-25"></div>
                </>
            ) : null}
        </>
    );
}
export default Modal;
